# Dall-E Local: Because Not Everyone Likes Colab and Jupyter

### This is a modified clone of the original [Dall-E Mini repo](https://github.com/borisdayma/dalle-mini).  It is designed to not only run locally without the need for Jupyter, but also download and save the model file.

## Setup

To build and install this repo and its prerequisites as a module run

```bash
python3 setup.py install --user
```

**Note**: The default requirements do not work with Cuda 11.3 or newer NVIDIA compute capabilities, so your mileage may vary for things working out of the box without massaging Python package versions.  

Tested on a 3080ti with Cuda 11.3 with the following package versions

```
$ pip freeze | grep -e torch -e transformers -e einops -e flax -e jax
einops==0.4.1
flax==0.5.0
jax==0.3.13
jaxlib==0.3.10+cuda11.cudnn82
pytorch-lightning==1.6.4
-e git+https://github.com/CompVis/taming-transformers@24268930bf1dce879235a7fddd0b2355b84d7ea6#egg=taming_transformers
torch==1.12.0+cu113
torch-fidelity==0.3.0
torchaudio==0.12.0+cu113
torchmetrics==0.6.0
torchvision==0.13.0
transformers==4.20.1
vqgan-jax==0.0.1
```

## Run Inference

The `local_inference.py` script can be run without any arguments and will produce 4 samples from the prompt "Sunset over a mountain lake".  Optional arguments are `--samples` which controls the number of sample produced, `--prompt` which changes the prompt used to generate samples, and `--output-path` which changes the default path where images are saved.

```bash
$ python3 local_inference.py --samples 2 --prompt "A 1950s TV with DJ Khaled on the screen"
```

Run with `-h` to display all the arguments.

```
$ python3 local_inference.py -h
usage: local_inference.py [-h] [--prompt PROMPT] [--output-path OUTPUT_PATH] [--samples SAMPLES]

optional arguments:
  -h, --help            show this help message and exit
  --prompt PROMPT       Image prompt
  --output-path OUTPUT_PATH
                        Output path where images will be saved. Default is ./output
  --samples SAMPLES     Number of images to generate. Default is 4.
```

## TODO

- [] Add an interactive option and use user input as prompt
- [] Add support for multiple prompts at once
- [] Try to speed up unloading of the model after completion
- [x] Test [Dalle-Mega](https://huggingface.co/dalle-mini/dalle-mega) 10GB model
  * Fails due to memory error, but not VRAM.
 - [x] Add dynamic model path input