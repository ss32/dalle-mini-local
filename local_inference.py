import argparse
from pathlib import Path
import os


def build_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--prompt", type=str, help="Image prompt", default="Sunset over a mountain lake"
    )
    parser.add_argument(
        "--output-path",
        type=str,
        help="Output path where images will be saved.  Default is ./output",
        default="./output",
    )
    parser.add_argument(
        "--samples",
        type=int,
        help="Number of images to generate.  Default is 4.",
        default=4,
    )
    parser.add_argument(
        "--model-path",
        type=str,
        help="Optional path to another model. Pass the directory to the model, not the file itself.  Contents should mirror the model directory here",
        default="./model"
    )
    return parser.parse_args()


def sanitize_image_name(output_path: Path, prompt: str):
    file_name_iterator = 0
    file_name = f"{prompt}_0.png"
    while os.path.exists(output_path / file_name):
        file_name_iterator += 1
        file_name = f"{prompt}_{file_name_iterator}.png"
    return file_name_iterator


args = build_args()

# Model Parameters
output_path = Path(args.output_path)
prompt = args.prompt

# dalle-mega
DALLE_MODEL = args.model_path
DALLE_COMMIT_ID = None

# VQGAN model
VQGAN_REPO = "dalle-mini/vqgan_imagenet_f16_16384"
VQGAN_COMMIT_ID = "e93a26e7707683d349bf5d5c41c5b0ef69b677a9"

import jax
import jax.numpy as jnp

jax.local_device_count()

if not os.path.exists(f"{DALLE_MODEL}/flax_model.msgpack"):
    print(f"{DALLE_MODEL}/flax_model.msgpack")
    DALLE_MODEL = "dalle-mini/dalle-mini/mega-1-fp16:latest"
    print("\nRunning a one-time download to get the model file.\n")
    print("#############################################")
    print(
        "When you are prompted to log in\nchoose option (3): Don't Visualize My Results"
    )
    print("#############################################")

# Load models & tokenizer
from dalle_mini import DalleBart, DalleBartProcessor
from vqgan_jax.modeling_flax_vqgan import VQModel
from transformers import CLIPProcessor, FlaxCLIPModel

# Load dalle-mini
print("Loading DALL-E Model...")
model, params = DalleBart.from_pretrained(
    DALLE_MODEL, revision=DALLE_COMMIT_ID, dtype=jnp.float16, _do_init=False
)

print("Loading VQGAN...")
# Load VQGAN
vqgan, vqgan_params = VQModel.from_pretrained(
    VQGAN_REPO, revision=VQGAN_COMMIT_ID, _do_init=False
)

"""Model parameters are replicated on each device for faster inference."""
from flax.jax_utils import replicate


params = replicate(params)
vqgan_params = replicate(vqgan_params)

"""Model functions are compiled and parallelized to take advantage of multiple devices."""
from functools import partial

# model inference
@partial(jax.pmap, axis_name="batch", static_broadcasted_argnums=(3, 4, 5, 6))
def p_generate(
    tokenized_prompt, key, params, top_k, top_p, temperature, condition_scale
):
    return model.generate(
        **tokenized_prompt,
        prng_key=key,
        params=params,
        top_k=top_k,
        top_p=top_p,
        temperature=temperature,
        condition_scale=condition_scale,
    )


# decode image
@partial(jax.pmap, axis_name="batch")
def p_decode(indices, params):
    return vqgan.decode_code(indices, params=params)


import random

# create a random key
seed = random.randint(0, 2**32 - 1)
key = jax.random.PRNGKey(seed)
processor = DalleBartProcessor.from_pretrained(DALLE_MODEL, revision=DALLE_COMMIT_ID)
tokenized_prompt = processor([prompt])
tokenized_prompt = replicate(tokenized_prompt)

# number of predictions
n_predictions = args.samples

# We can customize generation parameters
gen_top_k = None
gen_top_p = None
temperature = None
cond_scale = 3.0

from flax.training.common_utils import shard_prng_key
import numpy as np
from PIL import Image
from tqdm.notebook import trange

print(f"Prompt: {prompt}\n")
# generate images
images = []
for i in trange(max(n_predictions // jax.device_count(), 1)):
    # get a new key
    key, subkey = jax.random.split(key)
    # generate images
    encoded_images = p_generate(
        tokenized_prompt,
        shard_prng_key(subkey),
        params,
        gen_top_k,
        gen_top_p,
        temperature,
        cond_scale,
    )
    # remove BOS
    encoded_images = encoded_images.sequences[..., 1:]
    # decode images
    decoded_images = p_decode(encoded_images, vqgan_params)
    decoded_images = decoded_images.clip(0.0, 1.0).reshape((-1, 256, 256, 3))
    for decoded_img in decoded_images:
        img = Image.fromarray(np.asarray(decoded_img * 255, dtype=np.uint8))
        images.append(img)

# CLIP model
CLIP_REPO = "openai/clip-vit-base-patch32"
CLIP_COMMIT_ID = None

# Load CLIP
clip, clip_params = FlaxCLIPModel.from_pretrained(
    CLIP_REPO, revision=CLIP_COMMIT_ID, dtype=jnp.float16, _do_init=False
)
clip_processor = CLIPProcessor.from_pretrained(CLIP_REPO, revision=CLIP_COMMIT_ID)
clip_params = replicate(clip_params)

# score images
@partial(jax.pmap, axis_name="batch")
def p_clip(inputs, params):
    logits = clip(params=params, **inputs).logits_per_image
    return logits


from flax.training.common_utils import shard

# get clip scores
clip_inputs = clip_processor(
    text=[prompt] * jax.device_count(),
    images=images,
    return_tensors="np",
    padding="max_length",
    max_length=77,
    truncation=True,
).data
logits = p_clip(shard(clip_inputs), clip_params)
logits = logits.squeeze().flatten()

if not os.path.exists(output_path):
    os.makedirs(output_path)

print(f"Prompt: {prompt}\n")
prompt = prompt.replace(" ", "_")
file_name_offset = sanitize_image_name(output_path, prompt)
for idx in logits.argsort()[::-1]:
    file_name = f"{prompt}_{idx.tolist() + file_name_offset}.png"
    images[idx].save(output_path / file_name)
    print(f"Saved {output_path / file_name}, CLIP Score: {logits[idx]:.2f}\n")
